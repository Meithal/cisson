Cisson is a small self-contained JSON library in modern C. 
It parses JSON into an abstract tree and serializes trees 
into JSON. It lets you manipulate trees in various 
ways and build them from scratch. 

## Requirements
* C99 compiler. 

## Install
```c
#define JSN_IMPLEMENTATION
#include "cisson.h"
```

You can include cisson.h in more than one file, 
but you must define `JSN_IMPLEMENTATION` only 
once in all your code base.

Other ways than single-header to use the library are described in
[INSTALL](extras/doc/INSTALL.md).


## Examples
To turn the following C object into JSON

```C
int array[3] = {
  1, 2, 4
};
```

```C
#define JSN_IMPLEMENTATION
#include "cisson.h"

int main(void) {
    /* zero initializing the tree is enough for most use cases. */
    struct json_tree tree = {0};
    
    START_AND_PUSH_TOKEN(&tree, "[");
    PUSH_ROOT(&json_tree);
    /* every token we push is bound to a root,
     * "PUSH_ROOT" changes the current root to 
     * the last pushed token. */
    START_AND_PUSH_TOKEN(&tree, "1");
    START_AND_PUSH_TOKEN(&tree, "2");
    START_AND_PUSH_TOKEN(&tree, "4");
    
    puts(to_string(&json_tree)); /* [1,2,4] */
}
```

To extract values from JSON:

```c
#define JSN_IMPLEMENTATION
#include "cisson.h"

char * str = "{\"foo\":[1,2,3], \"bar\": null, "": {"": 24, " ": 42}}"

int main() {
    struct json_tree t = { 0 };
    rjson(str, &t); /* rjson reads json into a tree */
    puts(to_string_pointer(&t, query(&t, "/foo"))); /* [1,2,3] */
    puts(to_string_pointer(&t, query(&t, "/foo/0"))); /* 1 */
    puts(to_string_pointer(&t, query(&t, "/foo/1"))); /* 2 */
    puts(to_string_pointer(&t, query(&t, "/foo/2"))); /* 3 */
    puts(to_string_pointer(&t, query(&t, "/bar"))); /* null */
    puts(to_string_pointer(&t, query(&t, "/"))); /* {"": 24, " ": 42} */
    puts(to_string_pointer(&t, query(&t, "//"))); /* 24 */
    puts(to_string_pointer(&t, query(&t, "// "))); /* 42 */
    puts(to_string_pointer(&t, query(&t, ""))); /* {"foo":[1,2,3], "bar": null, "": {"": 24, " ": 42}} */
}
```

The syntax to address nodes is described by
[rfc6901](https://datatracker.ietf.org/doc/html/rfc6901).

## Testing
```shell
mkdir build
cd build
cmake ..
cmake --build . --target ALL_BUILD --config Release
ctest -C Release
```

Lecture
---

https://www.json.org

* Norms
  * JSON : https://datatracker.ietf.org/doc/rfc8259/
  * Internet JSON : https://www.rfc-editor.org/rfc/rfc7493.html
  * JSON pointer : https://www.rfc-editor.org/rfc/rfc6901
  * JSON patch : https://www.rfc-editor.org/rfc/rfc6902.html
  * JSON text sequences : https://www.rfc-editor.org/rfc/inline-errata/rfc7464.html
  * JSON merge patch : https://www.rfc-editor.org/rfc/rfc7396.html
  * Base64url : https://www.rfc-editor.org/rfc/rfc4648#section-5
  * JSON schema : 
    * https://json-schema.org
    * https://datatracker.ietf.org/doc/html/draft-bhutton-json-schema-01
    * https://datatracker.ietf.org/doc/html/draft-bhutton-json-schema-validation-01
  * JSON relative pointer : https://datatracker.ietf.org/doc/html/draft-bhutton-relative-json-pointer-00
  * JSON Path : https://goessner.net/articles/JsonPath/
  * JSON transform : https://goessner.net/articles/jsont/
* Conformance: http://seriot.ch/projects/parsing_json.html
* Performance : 
  * https://ibireme.github.io/yyjson/doc/doxygen/html/
  * https://github.com/simdjson/simdjson
* Extension :
  * JSON5 : https://json5.org
  * CSON : http://noe.mearie.org/cson/
  * CBOR : https://www.rfc-editor.org/rfc/inline-errata/rfc7049.html
  * Json-select : https://web.archive.org/web/20140208025525/http://jsonselect.org/