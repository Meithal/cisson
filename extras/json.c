#include "json.h"

#define X(a, b) [(a)] = (b),

static char whitespaces[] = {
    WHITESPACE
    '\xbf', // when we are at the final bom
};

#undef X

#undef WHITESPACE

static inline long long
in(const char* hay, char needle, int len) {
    for (int i = 0; i < len; i++) {
        if(hay[i] == needle) return i + 1;
    }
    return 0;
}
#define in_str(hay, needle) in((hay), (needle), sizeof (hay) - 1)
#define in_lit(hay, needle) in((hay), (needle), sizeof (hay))

static inline long long
len_whitespace(const char * string) {
    // todo: this function doest work in a bound
    //  checking environment
    long long count = 0;
    while(string && in_lit(whitespaces, string[count]) != 0) {
        count++;
    }
    return count;
}

static long long tok_index(struct json_token* stack, struct json_token* which) {
    return (which - stack);
}

/**
 * Better use START_AND_PUSH_TOKEN instead of this directly.
 * We store the length of the string next to it because unlike
 * for true, false... tokens that are fixed length, we dont know the length of variable
 * tokens, and we want to push the token only if we are sure
 * it is syntaxically correct. Since we are an incremental
 * parser, we dont know the length of the token we will push…
 * */
void start_string(long * cursor, char pool[]) {
    *cursor += pool[*cursor] + (long)sizeof (int);
    cs_memset(&pool[*cursor], 0, sizeof (int));
}

/** Better use START_AND_PUSH_TOKEN instead of this directly. */
EXPORT(void, push_string)(const long *cursor,
        char *pool,
        const char* string,
        long length) {
    int* sh = (int*)(void*)&pool[*cursor];
    cs_memcpy(
        pool + *cursor + sizeof (int) + *sh,
        string,
        length
    );
    *sh += (int)length;
}

/** Better use the CLOSE_ROOT macro instead of this. */
EXPORT(void, close_root)(struct json_token * tokens,
        long * root_index) {
    *root_index = tokens[*root_index].support_index;
}

/** Better use PUSH_ROOT macro instead of this. */
EXPORT(void, push_root)(long * root_index, const long * token_cursor) {
    *root_index = *token_cursor - 1;
}

static long long children_count(struct json_tree* tree, struct json_token* root) {
    long long count = 0;
    long long root_index = root - tree->stack;
    for(long long i = root_index + 1;
        i < tree->token_count && tree->stack[i].support_index >= root_index;
        i++) {
            count++;
    }

    return count;
}

/**
 Better use START_AND_PUSH_TOKEN instead of this directly.
 Never call this directly as it does not do bound checks.
 Those are done by a macro that can be disabled as to make
 bound checking optional.
 */
EXPORT(void, push_token_kind_)(
        char * address,
        struct json_tree *tree,
        long support_index,
        int length) {

    enum json_token_kind kind = (enum json_token_kind[]){
            JSK_UNSET,
            JSK_TRUE, JSK_FALSE,
            JSK_NULL, JSK_ARRAY, JSK_OBJECT,
            JSK_NUMBER, JSK_NUMBER, JSK_NUMBER,
            JSK_NUMBER, JSK_NUMBER, JSK_NUMBER,
            JSK_NUMBER, JSK_NUMBER, JSK_NUMBER,
            JSK_NUMBER, JSK_NUMBER,
            JSK_STRING,
    }[in_str("tfn[{-0123456789\"", *address)];

    if(*address == ':')
        return;

    /* move_token all the tokens coming after the direct children
     * of current support */
    const long cc = children_count(tree, &tree->stack[support_index]);
    /* all the tokens after the children of our root and before the max */
    const long to_move =
            (long)sizeof (struct json_token)
            * ((tree->token_count - 1) - (support_index + cc));
    cs_memmove(
        &tree->stack[support_index + cc + 2],
        &tree->stack[support_index + cc + 1],
        to_move
    );

    /* fix up support indices of moved tokens except for
     * the token that supports the others */
    for (long i = support_index + cc + 3; i < tree->token_count + 1; ++i) {
        tree->stack[i].support_index++;
    }

    struct json_token tok = {
        .kind=kind,
        .support_index=support_index,
        .address=address,
        .length=length,
    };

    tree->stack[support_index + cc + 1] = tok;

    tree->token_count++;
}

/**
 Inserts a token at the given root (fetched from query())
 use the insert_token() macro to not have to cast your string.
 */
EXPORT(void, insert_token_)(
        struct json_tree * tree, char *token, struct json_token* root) {

    long target_root = tok_index(tree->stack, root);

    if(token[0] == '>') {
        CLOSE_ROOT(tree);
        return;
    }

    START_AND_INSERT_TOKEN(tree, token, target_root);

    if(in_str("{[:", token[0])) {
        PUSH_ROOT(tree);
    }
}

static struct json_token tok_root;
static struct json_token tok_not_found;
static struct json_token tok_first;

static struct json_token* next_child__(
        struct json_tree * tree,
        struct json_token * root,
        struct json_token * current) {

    if(root == &tok_root) root = &tok_not_found;

    long root_idx = tok_index(tree->stack, root);
    long start = tok_index(tree->stack, current) + 1;

    if (current == &tok_first)
        start = (root_idx + 1);

    // we have to parse over nested arrays
    for(long i = start; i < tree->token_count && tree->stack[i].support_index >= root_idx; i++)
        if (tree->stack[i].support_index == root_idx) {
            return &tree->stack[i];
        }

    return &tok_not_found;
}

/**
 * Returns the next sibling from the given root and the given node.
 * Unlike the internal function, it tolerates NULLs from the userland.
 */
EXPORT(struct json_token* , next_child_)
(struct json_tree * tree,
 struct json_token * root,
 struct json_token * current) {
    // for outerland, that don't have access to tok_first
    if(!current) current = &tok_first;
    struct json_token * tok = next_child__(tree, root, current);
    if(tok == &tok_not_found) return (void*)0;

    return tok;
}

/**
 * Fetches a token from a json_tree with a json pointer.
 * Returns a falsy void pointer if not found.
 */
EXPORT(struct json_token*, query_from_)(
        struct json_tree * tree,
        long length,
        char * query,
        struct json_token * root) {
    /* todo : unescape ~0, ~1, ~2 */
    long i = 0;

    if(length == 0) {
        return root;
    }

    do {
        if(!root) {
            return root;
        }

        if (query[i] == '/') {
            if (root->kind != JSK_OBJECT && root->kind != JSK_ARRAY) {
                return (void*)0;
            }
            i++;
        }

        long tlen = 0;
        while (i + tlen < length && query[i + tlen] != '/') {
            tlen++;
        }

        char buffer[0x80] = { (char)('"' * (root->kind == JSK_OBJECT)) };
        cs_memcpy(&buffer[1ull * (root->kind == JSK_OBJECT)], &query[i], tlen);
        buffer[1 + tlen] = (char)('"' * (root->kind == JSK_OBJECT));

        if (root->kind == JSK_ARRAY) {
            int index = 0;
            int j;
            for (j = 0; buffer[j]; j++) {
                index *= 10;
                index += buffer[j] - '0';
            }
            struct json_token * cur = &tok_first;
            do {
                cur = next_child__(tree, root, cur);
            } while (cur != &tok_not_found && index--);
            root = (cur == &tok_not_found ? (void*)0 : cur);

        } else if (root->kind == JSK_OBJECT) {
            struct json_token * cur = &tok_first;
            while ((cur = next_child__(tree, root, cur)) != &tok_not_found) {
                if (cs_memcmp(cur->address, buffer, tlen + 2) == 0) {
                    if((i + tlen + 1 < length && /* deal with /< */
                        query[i + tlen] == '/' && query[i + tlen + 1] == '<')) {
                        return cur;
                    }
                    root = next_child__(tree, cur, &tok_first);
                    goto end;
                }
            }
            return (void*)0; /* if we haven't found a matching child */
        }
        end:i = i + tlen;
    } while (i++ < length);

    return root;
}

/**
 * Deletes a token and all its descendants
 */
EXPORT(void, delete_token_)(struct json_tree* tree, struct json_token* which) {
    /* todo: make a function that wipes bound string, in case we really want to delete, and not just move_token */
    long long cc = children_count(tree, which);
    long addr = which - tree->stack;
    cs_memmove(
       which,
       &tree->stack[addr + cc + 1],
       sizeof (struct json_token) * (tree->token_count - (addr + cc + 1))
               );
    tree->token_count -= (cc + 1);

    for(long i = addr; i < tree->token_count; i++) {
        if(tree->stack[i].support_index > tok_index(tree->stack, which)) {
            tree->stack[i].support_index -= (cc+1);
        }
    }
}

/**
 * Deletes a token and shifts all next ones, fix their support index.
 */
static void delete_one_token(struct json_tree* tree, struct json_token* which) {
    cs_memmove(
       which,
       &tree->stack[(which - tree->stack) + 1],
       sizeof (struct json_token) * ((tree->token_count - tok_index(tree->stack, which)) - 1)
    );
    tree->token_count --;
    for(long i = tok_index(tree->stack, which); i < tree->token_count; i++) {
        if(tree->stack[i].support_index > tok_index(tree->stack, which)) {
            tree->stack[i].support_index--;
        }
    }
}

/**
 * Returns support of a node, until the root or virtual
 * root is met (when printing from a pointer).
 */
static inline struct json_token *
support(struct json_token* stack,
        struct json_token* cur,
        struct json_token* host) {
    if(cur == &tok_root) return &tok_root;
    if(cur == host) return &tok_root;
    return &stack[cur->support_index];
}

/**
 Returns the container of a token or itself if it is one.
 The container is either root, an object, an array, or an array key.
 todo: this is flimsy and should be removed
 */
static struct json_token* container(struct json_token* stack, struct json_token* which) {
    if (which->support_index == ROOT_TOKEN
    || which->kind == JSK_ARRAY
    || which->kind == JSK_OBJECT
    || (
            which->kind == JSK_STRING
            && support(stack, which, stack)->kind == JSK_OBJECT
        )
    )
    {
        return which;
    }
    return support(stack, which, stack);
}

/** move_token tokens and fix offsets that need be. */
EXPORT(void, move_token_)(
        struct json_tree* state, struct json_token* which, struct json_token* where) {

    long root_support_index = which->support_index;
    do {
        char* address = which->address;

        long support_index = tok_index(
            state->stack,
            container(state->stack, where)
        );
        int length = which->length;
        // target support index is the target,
        // minus the original support distance, minus the nb of siblings

        delete_one_token(state, which);
        // at this point, which refers to a different token
        // so support index is - 1
        INSERT_TOKEN_LEN(address, state, support_index - 1, length);
    } while (
        which->support_index > root_support_index
        /* with the delete_one_token() which points to the next token thanks to the memmove */
    );
}

/** inserts a new string and makes the given token point to it. */
EXPORT(void, rename_string_)(
        struct json_tree* state, struct json_token* which, long len, const char* new_name) {
    START_STRING(state);
    PUSH_STRING(state, "\"", 1);
    PUSH_STRING(state, new_name, len);
    PUSH_STRING(state, "\"", 1);
    which->address = state->pool + state->pool_cursor + sizeof (int);
}

/* Injects JSON text inside an existing JSON json_tree. */
EXPORT(void, inject_)(
       struct json_tree * state,
       struct json_token * where) {
    // todo, we should be able to inject at the end of an array or object
    // with /- pointer, or anywhere inside an array or object.
    enum json_state old_state = state->cur_state;
    long old_root = state->current_support;

    // todo: we should error instead if target is not container.
    where = container(state->stack, where);
    state->current_support = (where - (state->stack));

    switch(state->stack[state->current_support].kind) {
       case JSK_OBJECT:
           state->cur_state = JSS_OBJECT_START; break;
       case JSK_ARRAY:
           state->cur_state = JSS_ARRAY_START; break;
       default:
           state->cur_state = JSS_DOC_START;
    }

    rjson_len(state->parser_state.len, state->parser_state.string, state);
    state->cur_state = old_state;
    state->current_support = old_root;
}

static inline struct json_token *
last_tok_support(struct json_tree* tree) {
    if(tree->current_support == ROOT_TOKEN) return &tok_root;
    // if we haven't pushed anything yet
    return &tree->stack[tree->current_support];
}

static inline char first_char_of_current_token(struct json_tree * tree) {
    return tree->pool[tree->pool_cursor + sizeof(int)];
}

static int is_last_char_in_number(struct json_tree* tree) {
    if(first_char_of_current_token(tree) == '"') {
        // current token is a string, not a number
        return 0;
    }

    if(tree->parser_state.cursor == tree->parser_state.len - 1) {
        return 1;
    }
    
    char chr = tree->parser_state.string[tree->parser_state.cursor + 1];
    // we are inbound here

    if(in_str(RAW_WHITESPACE ",]}\0", chr)) {
        return 1;
    }

    return 0;
}

/* build a dom tree, another builder might do a dry run instead */
static int dom_builder(struct json_tree* tree) {

    long advance_by = 1;

    switch (tree->cur_state) {
        case JSS_ERROR_STATE:
        case JSS_DOC_END:
        case JSS_BOM_2:
        case JSS_BOM_3:
        case JSS_TRUE_T:
        case JSS_TRUE_R:
        case JSS_TRUE_U:
        case JSS_FALSE_F:
        case JSS_FALSE_A:
        case JSS_FALSE_L:
        case JSS_FALSE_S:
        case JSS_NULL_N:
        case JSS_NULL_U:
        case JSS_NULL_L:
        case JSS_ARRAY_COMMA:
            break;
        case JSS_WS_BEFORE_VALUE:
        case JSS_WS_AFTER_VALUE_ROOT:
        case JSS_WS_AFTER_VALUE_ARRAY:
        case JSS_WS_AFTER_VALUE_OBJECT_KEY:
        case JSS_WS_AFTER_VALUE_OBJECT_VALUE:
        case JSS_OBJECT_WS_AFTER_OPEN:
        case JSS_ARRAY_WS_EXPECT_VALUE:
        case JSS_OBJECT_WS_AFTER_COMMA:
        case JSS_DOC_START:
            advance_by = len_whitespace(
                tree->parser_state.string + tree->parser_state.cursor
            );
            break;
        case JSS_TRUE_E:
            PUSH_TOKEN_LEN("true", 4, tree);
            break;
        case JSS_FALSE_E:
            PUSH_TOKEN_LEN("false", 5, tree);
            break;
        case JSS_NULL_LL:
            PUSH_TOKEN_LEN("null", 4, tree);
            break;
        case JSS_NUMBER_ZERO:
        case JSS_NUMBER_19:
        case JSS_NUMBER_MINUS:
        case JSS_STRING_START:
            START_STRING(tree);
        case JSS_NUMBER_EXPO_NUMBER:
        case JSS_NUMBER_MINUS_ZERO:
        case JSS_NUMBER:
        case JSS_NUMBER_COMMA_NUMBER:
        case JSS_NUMBER_EXPO_e:
        case JSS_NUMBER_EXPO_E:
        case JSS_NUMBER_COMMA:
        case JSS_NUMBER_EXPO_PLUS:
        case JSS_NUMBER_EXPO_MINUS:
        case JSS_IN_STRING:
        case JSS_STRING_ESCAPE_START:
        case JSS_ESCAPE_UNICODE_1:
        case JSS_ESCAPE_UNICODE_2:
        case JSS_ESCAPE_UNICODE_3:
        case JSS_ESCAPE_UNICODE_4:
        case JSS_ESCAPE_UNICODE_LAST:
        case JSS_STRING_CLOSE:
            PUSH_STRING(tree, tree->parser_state.string + tree->parser_state.cursor, 1);
            if(is_last_char_in_number(tree)) {
                PUSH_STRING_TOKEN(tree);
            }
            if(tree->cur_state == JSS_STRING_CLOSE) {
                PUSH_STRING_TOKEN(tree);
            }
            break;
        case JSS_ARRAY_START:
            PUSH_TOKEN_LEN("[", 1, tree);
            PUSH_ROOT(tree);
            break;
        case JSS_OBJECT_START:
            PUSH_TOKEN_LEN("{", 1, tree);
            PUSH_ROOT(tree);
            break;
        case JSS_OBJECT_COLON:
            PUSH_ROOT(tree);
            break;
        case JSS_OBJECT_CLOSE:
            if(last_tok_support(tree)->kind == JSK_STRING) {
                CLOSE_ROOT(tree); // close twice if we have one element
            }
        case JSS_OBJECT_COMMA:
        case JSS_ARRAY_CLOSE:
            CLOSE_ROOT(tree);
            break;
    }
    
    tree->parser_state.cursor += advance_by;
    
    if(tree->parser_error_handler(tree)) {
        return 1; // halt parsing
    }

    if(tree->parser_state.cursor == tree->parser_state.len) {
        return 1; // we are at the end of the string
    }

    return 0;
}

/* writes an error string, a different error handler might be more laxist
 * on some grammars to allow for trailing commas and so on */
static int default_parser_error_handler(struct json_tree* tree) {
    char chr[5] = {tree->parser_state.string[tree->parser_state.cursor - 1]};

    if(tree->cur_state == JSS_ERROR_STATE) {

        if(chr[0] < 20) {
            cs_memcpy(chr,
                    (&(char[5]){'0', 'x',
                                (chr[0] / 16) < 10 ? (chr[0] / 16) + '0' : (chr[0] / 16) - 9 + 'a',
                                (chr[0] % 16) < 10 ? (chr[0] % 16) + '0' : (chr[0] % 16) - 9 + 'a',0}),
                    5);
        }
        // We want to print at most 20 characters before
        long start_at =
            tree->parser_state.cursor > 20
            ? tree->parser_state.cursor - 20
            : 0;
        long to_print_before = tree->parser_state.cursor > 20 ? (tree->parser_state.cursor - start_at) : tree->parser_state.cursor;
        cs_memcpy(
            tree->parser_state.error_string,
            tree->parser_state.string + start_at,
            to_print_before);
        cs_memcpy(tree->parser_state.error_string + to_print_before, "\x1b[30;101m", 9);
        cs_memcpy(tree->parser_state.error_string + to_print_before + 9, chr, cs_strlen(chr));
        cs_memcpy(tree->parser_state.error_string + to_print_before + 9 + cs_strlen(chr), "\x1b[0m", 4);
        cs_memcpy(tree->parser_state.error_string + to_print_before + 9 + cs_strlen(chr) + 4, 
                  tree->parser_state.string + tree->parser_state.cursor + (tree->parser_state.len ? 1 : 0) ,
        1);

        return 1;
    }

    if(!tree->parser_bound_checker(tree)) {
        cs_memcpy(tree->parser_state.error_string , "No more memory", sizeof("No more memory"));
        tree->cur_state = JSS_ERROR_STATE;

        return 1;
    }

    if(tree->parser_state.cursor >= tree->parser_state.len) {
        // >= because on empty string, we advance anyway
        if(chr[0] == '\0' && tree->current_support > -1) {
            tree->previous_state = tree->cur_state;
            tree->cur_state = JSS_ERROR_STATE;
            tree->parser_state.cursor--;
            cs_memcpy(tree->parser_state.error_string , "Uncomplete token", sizeof("Uncomplete token"));
        }

        return 1;
    }

    return 0;
}

/* this assumes we use the default pool and stack */
static int default_bound_checker(struct json_tree* tree) {
    return (tree->token_count < CS_STATIC_STACK_SIZE
             && tree->pool_cursor < CS_STATIC_POOL_SIZE);
}

/* this parses the json grammar */
static int default_parser(struct json_tree* tree) {
    
    tree->previous_state = tree->cur_state;

    enum json_state state = 0;
    char chr = tree->parser_state.string[tree->parser_state.cursor];

    switch (tree->cur_state) {
        case JSS_ERROR_STATE: {
            break;
        }
        case JSS_DOC_END:
            state = JSS_ERROR_STATE;
            break;
        case JSS_DOC_START:
        case JSS_ARRAY_START:
        case JSS_WS_BEFORE_VALUE:
        case JSS_ARRAY_COMMA:
        case JSS_OBJECT_COLON:
        case JSS_ARRAY_WS_EXPECT_VALUE:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                (JSS_BOM_2 * (tree->cur_state == JSS_DOC_START)),
                // this array value equals error state if not at the initial state
                JSS_TRUE_T, JSS_FALSE_F, JSS_NULL_N,
                (enum json_state[]) {
                    [JSK_UNSET] = JSS_WS_BEFORE_VALUE,
                    [JSK_ARRAY] = JSS_ARRAY_WS_EXPECT_VALUE,
                    [JSK_STRING] = JSS_OBJECT_COLON,
                }[last_tok_support(tree)->kind],
                (enum json_state[]) {
                    [JSK_UNSET] = JSS_WS_BEFORE_VALUE,
                    [JSK_ARRAY] = JSS_ARRAY_WS_EXPECT_VALUE,
                    [JSK_STRING] = JSS_OBJECT_COLON,
                }[last_tok_support(tree)->kind],
                (enum json_state[]) {
                    [JSK_UNSET] = JSS_WS_BEFORE_VALUE,
                    [JSK_ARRAY] = JSS_ARRAY_WS_EXPECT_VALUE,
                    [JSK_STRING] = JSS_OBJECT_COLON,
                }[last_tok_support(tree)->kind],
                (enum json_state[]) {
                    [JSK_UNSET] = JSS_WS_BEFORE_VALUE,
                    [JSK_ARRAY] = JSS_ARRAY_WS_EXPECT_VALUE,
                    [JSK_STRING] = JSS_OBJECT_COLON,
                }[last_tok_support(tree)->kind],
                JSS_NUMBER_MINUS, JSS_NUMBER_ZERO,
                JSS_NUMBER_19, JSS_NUMBER_19,JSS_NUMBER_19,
                JSS_NUMBER_19,JSS_NUMBER_19,JSS_NUMBER_19,
                JSS_NUMBER_19,JSS_NUMBER_19,JSS_NUMBER_19,
                JSS_STRING_START,
                JSS_ARRAY_START,
                JSS_OBJECT_START,
                JSS_ARRAY_CLOSE * (tree->cur_state == JSS_ARRAY_START || tree->cur_state == JSS_ARRAY_WS_EXPECT_VALUE),
            }[in_str(RAW_BOM_STARTER "t" "f" "n" RAW_WHITESPACE "-" "0" RAW_DIGIT "\"[{]", chr)];
            break;
        
        case JSS_OBJECT_START:
        case JSS_OBJECT_WS_AFTER_OPEN:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_OBJECT_WS_AFTER_OPEN, JSS_OBJECT_WS_AFTER_OPEN,
                JSS_OBJECT_WS_AFTER_OPEN, JSS_OBJECT_WS_AFTER_OPEN,
                JSS_STRING_START,
                JSS_OBJECT_CLOSE * (tree->cur_state != JSS_OBJECT_COMMA),
            }[in_str(RAW_WHITESPACE "\"}", chr)];
            break;
        case JSS_OBJECT_COMMA:
        case JSS_OBJECT_WS_AFTER_COMMA:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_OBJECT_WS_AFTER_COMMA, JSS_OBJECT_WS_AFTER_COMMA,
                JSS_OBJECT_WS_AFTER_COMMA, JSS_OBJECT_WS_AFTER_COMMA,
                JSS_STRING_START,
            }[in_str(RAW_WHITESPACE "\"", chr)];
            break;
        case JSS_BOM_2: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_BOM_3
            }[RAW_BOM[1] == chr];
            break;
        }
        case JSS_BOM_3: {
            // fixme, should be in same branch as other expecters
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_WS_BEFORE_VALUE
            }[RAW_BOM[2] == chr];
            break;
        }
        case JSS_TRUE_T: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_TRUE_R
            }['r' == chr];
            break;
        }
        case JSS_TRUE_R: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_TRUE_U
            }['u' == chr];
            break;
        }
        case JSS_TRUE_U: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_TRUE_E
            }['e' == chr];
            break;
        }
        case JSS_FALSE_F: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_FALSE_A
            }['a' == chr];
            break;
        }
        case JSS_FALSE_A: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_FALSE_L
            }['l' == chr];
            break;
        }
        case JSS_FALSE_L: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_FALSE_S
            }['s' == chr];
            break;
        }
        case JSS_FALSE_S: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_FALSE_E
            }['e' == chr];
            break;
        }
        case JSS_NULL_N: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_NULL_U
            }['u' == chr];
            break;
        }
        case JSS_NULL_U: {
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_NULL_L
            }['l' == chr];
            break;
        }
        case JSS_NULL_L: {
            state = (enum json_state[]) {
                    JSS_ERROR_STATE,
                    JSS_NULL_LL
            }['l' == chr];
            break;
        }
        case JSS_TRUE_E:
        case JSS_FALSE_E:
        case JSS_NULL_LL:
        case JSS_STRING_CLOSE:
        case JSS_ARRAY_CLOSE:
        case JSS_OBJECT_CLOSE:
        case JSS_WS_AFTER_VALUE_ROOT:
        case JSS_WS_AFTER_VALUE_ARRAY:
        case JSS_WS_AFTER_VALUE_OBJECT_KEY:
        case JSS_WS_AFTER_VALUE_OBJECT_VALUE:
        case JSS_NUMBER_MINUS_ZERO:
        case JSS_NUMBER_ZERO:
        case JSS_NUMBER_19:
        case JSS_NUMBER:
        case JSS_NUMBER_COMMA_NUMBER:
        case JSS_NUMBER_EXPO_NUMBER:

            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                (enum json_state[]){
                    JSS_ERROR_STATE,
                    JSS_WS_AFTER_VALUE_ROOT,
                    JSS_WS_AFTER_VALUE_ARRAY,
                    JSS_WS_AFTER_VALUE_OBJECT_KEY,
                    JSS_WS_AFTER_VALUE_OBJECT_VALUE,
                }[
                    (int[]){
                        [JSK_UNSET] = 1,
                        [JSK_ARRAY] = 2,
                        [JSK_OBJECT] = 3,
                        [JSK_STRING] = 4,
                        [JSK_NULL] = 0,
                        [JSK_TRUE] = 0,
                        [JSK_FALSE] = 0,
                        [JSK_NUMBER] = 0,
                    }[last_tok_support(tree)->kind]
                ],
                JSS_ARRAY_COMMA,
                JSS_OBJECT_COMMA,
                JSS_ARRAY_CLOSE,
                JSS_OBJECT_CLOSE,
                JSS_OBJECT_COLON,
                JSS_NUMBER_COMMA ,
                JSS_NUMBER_EXPO_e,
                JSS_NUMBER_EXPO_E,
                JSS_NUMBER,
                JSS_NUMBER_COMMA_NUMBER,
                JSS_NUMBER_EXPO_NUMBER,
                JSS_DOC_END,
            }[
                1 * (_Bool)in_str(RAW_WHITESPACE, chr)
                + 2 * (',' == chr && (last_tok_support(tree)->kind == JSK_ARRAY))
                + 3 * (',' == chr && (last_tok_support(tree)->kind == JSK_STRING))
                + 4 * (']' == chr && (last_tok_support(tree)->kind == JSK_ARRAY))
                + 5 * ('}' == chr && (last_tok_support(tree)->kind == JSK_STRING))
                + 6 * (':' == chr && (last_tok_support(tree)->kind == JSK_OBJECT))
                + 7 * ('.' == chr) * (_Bool)in_lit(((char[]){
                    JSS_NUMBER_19, JSS_NUMBER,
                    JSS_NUMBER_ZERO, JSS_NUMBER_MINUS_ZERO
                }), tree->cur_state)
                + 8 * ('e' == chr) * (_Bool)in_lit(((char[]){
                    JSS_NUMBER_19, JSS_NUMBER,
                    JSS_NUMBER_ZERO, JSS_NUMBER_MINUS_ZERO,
                    JSS_NUMBER_COMMA_NUMBER
                }), tree->cur_state)
                + 9 * ('E' == chr) * (_Bool)in_lit(((char[]){
                    JSS_NUMBER_19, JSS_NUMBER,
                    JSS_NUMBER_ZERO, JSS_NUMBER_MINUS_ZERO,
                    JSS_NUMBER_COMMA_NUMBER
                }), tree->cur_state)
                + 10 * (in_str( "0" RAW_DIGIT, chr) && in_lit(((char[]){
                        JSS_NUMBER_19, JSS_NUMBER
                }), tree->cur_state))
                + 11 * (in_str( "0" RAW_DIGIT, chr) && tree->cur_state == JSS_NUMBER_COMMA_NUMBER)
                + 12 * (in_str( "0" RAW_DIGIT, chr) && tree->cur_state == JSS_NUMBER_EXPO_NUMBER)
                + 13 * ((chr == '\0') && tree->current_support == -1)
            ];
            
            break;
        case JSS_NUMBER_MINUS:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_NUMBER_MINUS_ZERO,
                JSS_NUMBER, JSS_NUMBER, JSS_NUMBER,
                JSS_NUMBER,JSS_NUMBER,JSS_NUMBER,
                JSS_NUMBER,JSS_NUMBER,JSS_NUMBER,
            }[in_str( "0" RAW_DIGIT, chr)];
            break;
        case JSS_NUMBER_COMMA:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_NUMBER_COMMA_NUMBER,
            }[(_Bool)in_str("0" RAW_DIGIT, chr)];
            break;
        case JSS_NUMBER_EXPO_e:
        case JSS_NUMBER_EXPO_E:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_NUMBER_EXPO_MINUS, JSS_NUMBER_EXPO_PLUS,
                JSS_NUMBER_EXPO_NUMBER,JSS_NUMBER_EXPO_NUMBER,
                JSS_NUMBER_EXPO_NUMBER,JSS_NUMBER_EXPO_NUMBER,
                JSS_NUMBER_EXPO_NUMBER,JSS_NUMBER_EXPO_NUMBER,
                JSS_NUMBER_EXPO_NUMBER,JSS_NUMBER_EXPO_NUMBER,
                JSS_NUMBER_EXPO_NUMBER, JSS_NUMBER_EXPO_NUMBER,
            }[in_str( "-+0" RAW_DIGIT, chr)];
            break;
        case JSS_NUMBER_EXPO_MINUS:
        case JSS_NUMBER_EXPO_PLUS:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_NUMBER_EXPO_NUMBER
            }[(_Bool)in_str( "0" RAW_DIGIT, chr)];
            break;
        case JSS_STRING_START:
        case JSS_IN_STRING:
        case JSS_ESCAPE_UNICODE_LAST:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_STRING_CLOSE,
                JSS_STRING_ESCAPE_START,
                JSS_IN_STRING,
            }[
                1 * in_str("\"", chr)
                + 2 * in_str("\\", chr)
                + 3 * ((chr >= ' ' || chr < 0) && chr != '"' && chr != '\\') /* any non control ascii char */
            ];
            
            break;
            
        case JSS_STRING_ESCAPE_START:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_IN_STRING,
                JSS_ESCAPE_UNICODE_1,
            }[
                1 * (_Bool)in_str("\"\\/bfnrt", chr)
                + 2 * (chr == 'u')
            ];

            break;
        case JSS_ESCAPE_UNICODE_1:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_ESCAPE_UNICODE_2
            }[(_Bool)in_str("ABCDEFabcdef0" RAW_DIGIT, chr)];
            
            break;
        case JSS_ESCAPE_UNICODE_2:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_ESCAPE_UNICODE_3
            }[(_Bool)in_str("ABCDEFabcdef0" RAW_DIGIT, chr)];
            
            break;
        case JSS_ESCAPE_UNICODE_3:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_ESCAPE_UNICODE_4
            }[(_Bool)in_str("ABCDEFabcdef0" RAW_DIGIT, chr)];
            
            break;
        case JSS_ESCAPE_UNICODE_4:
            state = (enum json_state[]) {
                JSS_ERROR_STATE,
                JSS_ESCAPE_UNICODE_LAST
            }[(_Bool)in_str("ABCDEFabcdef0" RAW_DIGIT, chr)];
            
            break;
    }

    tree->cur_state = state;
    
    return tree->parser_processor(tree);
}

static void convenience(struct json_tree * state)
{
    state->cur_state = JSS_DOC_START;
    state->stack = state->stack ? state->stack : static_stack;
    state->pool = state->pool ? state->pool : static_pool;
    state->current_support = ROOT_TOKEN;
    state->parser = state->parser ? state->parser : default_parser;
    state->parser_processor = state->parser_processor ? state->parser_processor : dom_builder;
    state->parser_error_handler =     state->parser_error_handler ? state->parser_error_handler : default_parser_error_handler;
    state->parser_bound_checker = state->parser_bound_checker ? state->parser_bound_checker : default_bound_checker;
}


/**
 Streams characters into a json_tree without checking,
 like (json_tree, target, '~', "1~2~3")
*/
EXPORT(void, stream_tokens_)(
        struct json_tree * state, struct json_token * where,
        char separator, char stream[], long length) {

    long i = 0;
    long old_root = state->current_support;
    state->current_support = state->stack ? (where - state->stack) : -1;

    while (i < length) {
        long token_length = 0;
        while (i + token_length < length && stream[i + token_length] != separator) {
            token_length++;
        }
        stream[i + token_length] = '\0';
        push_token(state, &stream[i]);
        i = i + token_length + (long)sizeof separator;
    }
    state->current_support = old_root;
}


/**
 Parses JSON text .
 */
EXPORT(void, rjson_)(struct json_tree * state) {
    
    if (state->parser == 0) {
        convenience(state);
    }

    // todo: fully test reentry,
    // todo: implement streaming (SAX) parser
    // todo: add jasmine mode? aka not copy strings+numbers ?
    // todo: pedantic mode? (forbid duplicate keys, enforce order, cap integer values to INT_MAX...)
    // todo: test for overflows (deeply nested [[[[... > 2 GB)
    // fixme: check for bounds
    // todo: no memory move_token mode (simple tokenizer)
    // todo: implement JSON pointer RFC6901
    // todo: implement JSON schema
    // todo: output a html page where json pointer can be used as an anchor
    // todo: support codepoints > 0x8000 (surrogate pairs)
    // todo: experiment with watcher root token, to reduce null checks and performace

    for(;;) {
        if(state->parser(state)) goto exit;
    }

    exit: return;
}

static char ident_s[0x80];
static char * print_ident(int depth, long indent) {
    if (!depth) return (char*)"";
    cs_memset(ident_s, ' ', (indent * depth));
    ident_s[(indent * depth)] = '\0';
    return ident_s;
}

static struct json_token * climb_up(
        struct json_token* tokens,
        struct json_token** which,
        struct json_token** container,
        struct json_token* limit) {
    struct json_token* immediate = support(tokens, *container, limit);
    if(immediate==&tok_root) {*which = immediate; *container = &tok_root; goto end;}
    if(immediate->kind == JSK_ARRAY || immediate->kind == JSK_OBJECT)
    { // checkme: codecov
        *which = *container;
        *container = immediate;
    }
    if(immediate->kind == JSK_STRING) {
        *container = support(tokens, immediate, limit);
        *which = immediate;
    }
    end:return *container;
}

static char output_[CS_STATIC_POOL_SIZE];

/* prints JSON text into the sink, better use to_string*
 * macros instead of this directly. */
EXPORT(char * , to_string_)(
        struct json_tree * tree,
        struct json_token * start_token,
        int indent,
        char* sink,
        int sink_size) {

    char * output = output_;
    if(sink) {
        output = sink;
    } else {
        sink_size = sizeof output_;
    }

    #ifndef NDEBUG
        cs_memset(output, 0, sink_size);
    #endif

    long cursor = 0;

    struct json_token *stack = tree->stack;

    if (start_token == (void*)0) {
        start_token = &stack[0];
    }

    int depth = 0;

    struct json_token * tok;
    #define idx(tok_) tok_index(stack, tok_)
    for(tok = start_token;
        tok == start_token
        || (
            idx(tok) < tree->token_count
            && tok->support_index > start_token->support_index
            && (container(stack, start_token) == start_token)
            ); // whether started with a container or itself
        tok = &stack[idx(tok) + 1]
    ) {
    #undef idx
    #define cat_raw(where, string) ( \
        cs_memcpy((where), (string), cs_strlen((string))), cs_strlen((string)) \
    )

        if (support(stack, tok, stack)->kind != JSK_STRING) {
            cursor += cat_raw(output + cursor, print_ident(depth, indent));
        }

        long len = tok_len(tok);

        cs_memcpy(output+cursor, tok->address, len);
        cursor += len;

        if (support(stack, tok, stack)->kind == JSK_OBJECT && tok->kind == JSK_STRING) {
            cursor += cat_raw(output + cursor, indent ? ": " :  ":");
        }

        if(tok->kind == JSK_ARRAY || tok->kind == JSK_OBJECT) {
            cursor += cat_raw(output + cursor, !indent ? "" : "\n");
            depth++;
        }

        if(next_child__(tree, tok, &tok_first) != &tok_not_found) {
            continue; /* if we have children */
        }

        struct json_token * cur = tok;
        struct json_token * sup = support(stack, cur, start_token);

        /* check we are container w/o children */
        if((cur->kind == JSK_ARRAY || cur->kind == JSK_OBJECT)
            && next_child__(tree, cur, &tok_first) == &tok_not_found)
        {
            sup = cur;
            cur = &(struct json_token) {0};
        }

        if(sup && sup->kind == JSK_STRING) {
            cur = sup;
            sup = support(stack, sup, start_token);
        }

        // close ]} groups and print them at correct indents
        while (
                sup != &tok_root /* or check we are last child of our parent container */
                && next_child__(tree, sup, cur) == &tok_not_found
        ) {
            cursor += cat_raw(output + cursor, !indent ? "" : "\n");

            --depth, depth < 0 ? depth = 0 : 0;
            cursor += cat_raw(
                output + cursor, print_ident(depth, indent)
            );
            cursor += cat_raw(output + cursor, ((char[2]) {
                "\0]}"[
                    in_lit(
                        ((char[]){JSK_ARRAY, JSK_OBJECT}),
                        sup ? (int)sup->kind : 0
                    )
                ],
                '\0'
            }));

            climb_up(tree->stack, &cur, &sup, start_token);
        }

        if (sup != &tok_root
            && next_child__(tree, sup, cur) != &tok_not_found) {
            cursor += cat_raw(output + cursor,
                              indent ? ",\n" : ",");
        }
    }

    output[cursor] = '\0';
    return output;
    #undef cat_raw
}

#undef EXPORT
