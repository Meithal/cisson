8 september 2023
---

The parser is now modularizable, whith an AST
building module providing the same service as the 
previous hardcoded parser.

The new parser doesn't store true, false and null tokens
as strings, but only as tokens, the serializer 
has been changed to print null, true and false whenever
a token af that kind is encountered.

Added JS* prefix to most globals.