# Install

`cisson.h` is the result of concatenating `json.h` and `json.c`.

If you use cisson.h, you will have to define `JSN_IMPLEMENTATION` before
including it.

```c
#define JSN_IMPLEMENTATION
#include "cisson.h"
```
This will make the preprocessor include the implementation of the library
in the file that use it. If you need to have json functions in more
than one place you can still include cisson.h, but do not define
`JSN_IMPLEMENTATION` or you will have functions defined more than once.

You can also use `json.h`
```c
#include "json.h"
```
and compile/link `json.c` yourself with a makefile or similar.

The `CMakeLists.txt` provides 2 build targets that you can
reuse: `xjson` and `sjson`

Various examples on how to use cisson through a shared/static/baked way
are provided in the root CMakeLists.txt.

```cmake
add_subdirectory(cisson)
target_link_libraries(your_executable_target sjson)
```
Will build the library as a static lib and bundle it in your executable.

```cmake
add_subdirectory(cisson)
target_link_libraries(your_executable_target xjson)
```
Will build the library as a .dll/.dynlib/.so, you may need to move
the final dll in the same folder as the executable that uses it.

```cmake
add_subdirectory(cisson)
target_sources(your_executable_target ${JSON_FILES})
```
Will bake the library inside the executable you specify.

## Preprocessor options
* `CS_WANT_LIBC` includes the standard C library instead of
  drop-in replacement functions which may be less optimized.
* `CS_STATIC_STACK_SIZE` the size of the static tokens stack,
  set it to 1 if you dont need it and will allocate your
  stack yourself, or to a bigger value if you find yourself
  out of memory parsing large json documents.
* 